<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;

class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{
	/** @var Nette\Database\Context */
	private $database;


	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}


	/**
	 * Performs an authentication.
	 * @param array
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
        	list($username, $password) = $credentials;
    
            $selection =  $this->database->table('users');
            $users = $selection->where('username', $username);
         
            $user = $users->fetch(); 
         
        
		if (!$user) {
			throw new Nette\Security\AuthenticationException('The email is incorrect.', self::IDENTITY_NOT_FOUND);
    
		}else{
              $hash = Passwords::hash($user->password);   	
        }          
        
        if (!Passwords::verify($password,$hash)) {    
            throw new Nette\Security\AuthenticationException('The password is incorrect--', self::INVALID_CREDENTIAL);           
		} 
		$arr = $user->toArray();
		unset($arr['password']);
		return new Nette\Security\Identity($user['username'], $user['role'], $arr);

    }
}

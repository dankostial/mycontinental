<?php

namespace App\Model;

use Nette;

abstract class BaseModel extends Nette\Object {
	/** @var Nette\Database\Context */
	protected $db;

	/** @var string */
	public $tableName;

	public function __construct(Nette\Database\Context $db) {
		$this->db = $db;
	}

	/**
	 * Get all records in table
	 * @return Nette\Database\Table\Selection
	 */
	public function getAll() {
		return $this->db->table($this->tableName);
	}

	/**
	 * Get single row by id
	 * @param $id
	 * @throws NotFoundModelException
	 * @return Nette\Database\Table\ActiveRow
	 */
	public function getById($id) {
		$row = $this->getAll()->get($id);
		if ($row == False) {
			throw new NotFoundModelException;
		}

		return $row;
	}

	/**
	 * Insert new row to table
	 * @param array
	 * @return Nette\Database\Table\ActiveRow
	 */
	public function insert($rowData) {
		return $this->getAll()->insert($rowData);
	}

	/**
	 * Remove row with id
	 * @param $id
	 * @throws NotFoundModelException
	 */
	public function deleteById($id) {
		$row = $this->getAll()->get($id);
		$row->delete();
	}
}

class ModelException extends \Exception {
}

class NotFoundModelException extends ModelException {
}

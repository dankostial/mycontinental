<?php

namespace App\Model\Security;

use Nette;
use Nette\Security\Permission;
use App\Model\EmployeeModel;

class Authorizator extends Permission {
	public function __construct() {
		// Setup roles
     
		// Setup resources
		$this->addResource("Homepage");
 		$this->addResource("Calendar");
 		$this->addResource("Attendance");
 		$this->addResource("Configuration");
 		$this->addResource("Profile");
 		$this->addResource("Market");
		$this->addResource("Canteen");
		$this->addResource("Workers");
		



		// Setup permissions
	       
  		$this->addRole('worker');     
  		$this->addRole('majster');     
          
              
    	$this->allow('majster', "Homepage");
    	$this->allow('majster', "Calendar");
    	$this->allow('majster', "Configuration");
    	$this->allow('majster', "Profile");
    	$this->allow('majster', "Workers");

                     
    	$this->allow('worker', "Homepage");
    	$this->allow('worker', "Calendar");
    	$this->allow('worker', "Profile");
    	$this->allow('worker', "Attendance");
    	$this->allow('worker', "Market");
    	$this->allow('worker', "Canteen");
        
              
  //  	$this->allow(worker', "Homepage", array("default"));       

        
        /*
		$this->allow(EmployeeModel::ROLE_EMPLOYEE, "Employee", array("detail", "edit", "default"));
		$this->allow(EmployeeModel::ROLE_EMPLOYEE, "Department", array("default", "detail"));
		$this->allow(EmployeeModel::ROLE_EMPLOYEE, "Job", array("default", "detail", "issueAnInvoice", "requirementDetail", "invoiceDetail", "setInvoiceState"));
		$this->allow(EmployeeModel::ROLE_EMPLOYEE, "Client", array("default", "detail"));

		$this->allow(EmployeeModel::ROLE_DEPARTMENT_MANAGER, "Employee", array("default", "detailOther"));
		$this->allow(EmployeeModel::ROLE_DEPARTMENT_MANAGER, "Department", array("detailOther", "removeEmployee", "addEmployee", "edit", "create", "manageRequirementAssignment"));
		$this->allow(EmployeeModel::ROLE_DEPARTMENT_MANAGER, "Client", array("detail", "edit", "remove", "add"));
		$this->allow(EmployeeModel::ROLE_DEPARTMENT_MANAGER, "Job", array("create", "edit", "addRequirement", "editRequirement", "removeRequirement"));

		$this->allow(EmployeeModel::ROLE_MANAGER, "Employee", array("add", "editRole", "create", "fire", "setSalary"));
		$this->allow(EmployeeModel::ROLE_MANAGER, "Department", array("removeEmployeeOther", "addEmployeeOther"));
		$this->allow(EmployeeModel::ROLE_MANAGER, "Job", array("issueAnInvoiceOther", "removeInvoice"));

		// Allow everything to admin user
		$this->allow(EmployeeModel::ROLE_ADMIN, self::ALL, self::ALL); 
        
        */
    
	}
}

<?php

namespace App\Model;

use Nette;



class MarketModel
{

    private $database;

    public function getValue()
    {
        return 5;
 
    }


    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function getAllUsers()
    {
        return $this->database->table('users');
 
    }
    
    public function getAllDeals()
    {
       
		$deals = $this->database->table('deals'); 
		$q = $this->database->query('SELECT t1.id_offer, t1.dateA, t1.shiftA, t1.dateB, t1.shiftB, t1.recompense, t2.username, t2.id_user FROM offers t1 INNER JOIN users t2 ON t1.id_user = t2.id_user');		  
		return $q;
 
    }
 
 
    
    
    
    public function newCandidate($id_offer,$id_user)
    {

		$this->database->query('INSERT INTO candidate (id_offer,id_user ) VALUES ( ?,?)',$id_offer,$id_user);
		  

    }

	
	public function getMyCandidates($id_user){
		
		$q = $this->database->query('SELECT t2.*, t3.username, t1.id_candidate, t1.accepted_user, t1.accepted_majster FROM candidate t1 
									JOIN offers t2 ON t1.id_offer = t2.id_offer 
									JOIN users t3 ON t2.id_user = t3.id_user 
									where t1.id_user = ?',$id_user);
			
		return $q;
	}
    
    public function getMyActiveCandidates($id_user){
		
		$q = $this->database->query('SELECT t2.*, t3.username, t1.id_candidate, t1.accepted_user, t1.accepted_majster FROM candidate t1 
									RIGHT JOIN offers t2 ON t1.id_offer = t2.id_offer 
									LEFT JOIN users t3 ON t1.id_user = t3.id_user 
									where t2.id_user = ?',$id_user);
			
		return $q;
	}
    
  
    
    
    public function getMyOffers($id_user){
		
		$q = $this->database->query('SELECT * FROM offers WHERE id_user = ?',$id_user);
		return $q;
		
	}
    

	public function deleteOffer($id_offer){

		$this->database->query('DELETE FROM offers WHERE id_offer = ?' ,$id_offer);
		$this->database->query('DELETE FROM candidate WHERE id_offer = ?' ,$id_offer);
				
	}

	public function refuseCandidate($id_candidate){

		$this->database->query('UPDATE candidate SET accepted_user = 0  WHERE id_candidate = ?' ,$id_candidate);
				
	}


    public function IsCandidateAccepted($id_offer){
		
		$q = $this->database->query('SELECT id_candidate FROM candidate  WHERE accepted_user = ? AND id_offer = ?' ,"1",$id_offer);
		return $q;
		
	}

    
    public function acceptCandidate($id_candidate){
		
		$this->database->query('UPDATE candidate SET accepted_user = 1  WHERE id_candidate = ?' ,$id_candidate);
		
	}

    public function deleteCandidate($id_candidate){
		
		$this->database->query('DELETE FROM candidate WHERE id_candidate = ?' ,$id_candidate);
		
		
	}
	
    public function createOffer($id_user, $dateA, $shiftA, $recompense){
		
		$this->database->query('INSERT INTO offers (id_user,dateA,shiftA,recompense ) VALUES ( ?,?,?,?)',$id_user, $dateA, $shiftA, $recompense);
		
		
	}	
	
    



}

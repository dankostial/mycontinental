<?php

namespace App\Model;

use Nette;



class CanteenModel
{

    private $database;



    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

 
    
    public function getWeekMenu($day)
    {
       
		$q = $this->database->query('SELECT  f.* FROM canteen_week c LEFT JOIN foods f ON  c.id_food = f.id_food WHERE c.id_day = ?',$day);		  
	
		return $q;
		
    }
 
 
    

	
    



}

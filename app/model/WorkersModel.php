<?php

namespace App\Model;

use Nette;



class WorkersModel
{

    private $database;




    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }



	public function getMyEmployee($id_group){
		
		$q = $this->database->query('SELECT * FROM users WHERE role = ? AND id_group = ?',"worker",$id_group);
		return $q;
		
	} 

	public function getCandidates($id_group){
		
		$q = $this->database->query('SELECT u.*  FROM users u RIGHT JOIN candidate ON u.id_user = candidate.id_user  
									WHERE role = ? AND id_group = ? 
									AND accepted_user = ?',"worker",$id_group,"1");
		return $q;
		
	} 


	public function acceptCandidate($id_candidate){

		$this->database->query('UPDATE candidate SET accepted_majster = 1  WHERE id_user = ?' ,$id_candidate);


	}


}

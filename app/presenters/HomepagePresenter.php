<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Presenter;
use Nette\Application\UI;

use App\Model\HomepageModel;

  
class HomepagePresenter extends ProtectedPresenter
{
                                       
	private $homepageModel;
    private $database;
    
    
   
    public function __construct(homepageModel $homepageModel)
    {
        $this->homepageModel = $homepageModel;
    }


    public function renderDefault()
    {


        $all_users = $this->homepageModel->getAllUsers();        
        $this->template->users =  $all_users;
        
        $user = $this->getUser();
    
        $all_deals = $this->homepageModel->getAllDeals($user);
        $this->template->deals =  $all_deals;
     
    }

    protected function createComponentShiftChange()
    {
        $form = new UI\Form;
        $form->addSubmit('login', 'Suhlasim');
        $form->onSuccess[] = $this->registrationFormSucceeded;
        return $form;
    }


    public function registrationFormSucceeded(UI\Form $form, $values)
    {
        $this->flashMessage('Byl jste úspešne registrován.');
        $this->redirect('Homepage:');
    }






    public function renderDelete($who){
     
              $this->usersModel->deleteUser($who);
     
              $this->redirect('Users:');         
              
    }

    

    
  

  
       

}

                                              
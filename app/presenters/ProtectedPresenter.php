<?php

//todo pri ajaxoch ($this->isAjax) nevypisovat chybu ale vratit napr http forbidden

namespace App\Presenters;

use Nette;
use Nette\Application\ForbiddenRequestException;

abstract class ProtectedPresenter extends BasePresenter {
	const authenticationEnabled = True;
	const authorizationEnabled = True;

	/**
	 * @var Nette\Security\User
	 * @inject
	 */
	public $user;

	/**
	 * Checks user's authentization and authorization
	 * @throws ForbiddenRequestException
	 */
	public function startup()
	{
		parent::startup();

		if (!self::authenticationEnabled) {
			return;
		}
		if (!$this->user->isLoggedIn()) {
			$this->redirect("Sign:in");
		}

		if (!self::authorizationEnabled) {
			return;
		}
                
        
		if (!$this->user->isAllowed($this->name, $this->action)) {
			
			$this->flashMessage("Access denied", "danger");
			
			$this->redirect("Sign:in");
		}
	}
}


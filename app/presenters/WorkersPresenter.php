<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Presenter;
use App\Model\WorkersModel;
use Nette\Utils\Arrays;
    
use Nette\Application\UI\Form;
use Nette\Forms\Controls;

  


  
class WorkersPresenter extends ProtectedPresenter
{
	
	public function __construct(workersModel $workersModel)
    {
        $this->workersModel = $workersModel;
        
    }    
                                       
    
    private $workersModel;
    private $database;
    
    
    
    
    
    public function renderDefault()
    {
		
		$id_user = $this->user->getIdentity()->id_user;
		$id_user_group = $this->user->getIdentity()->id_group;
		
		
		$this->template->workers = $this->workersModel->getMyEmployee($id_user_group);
		$this->template->candidates = $this->workersModel->getCandidates($id_user_group);

		

	
    }


    public function handleAcceptCandidate($id_candidate){


			$this->workersModel->AcceptCandidate($id_candidate);
	
		
	}


    
    
    
    
    
    
    
    
    
    
    
    
    
    
      

}


<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Presenter;
use App\Model\CanteenModel;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;   
use Nette\Application\UI\Form;
use Nette\Forms\Controls;
  
  
class CanteenPresenter extends ProtectedPresenter
{
	
    private $canteenModel; 	
	
    public function __construct(canteenModel $canteenModel)
    {
        $this->canteenModel = $canteenModel;    
    }                                       
   
 
    
	public function renderDefault()
    { 
    

        $this->template->monday = $this->canteenModel->getWeekMenu(1);
        $this->template->tuesday = $this->canteenModel->getWeekMenu(2);
        $this->template->wednesday = $this->canteenModel->getWeekMenu(3);
        $this->template->fourday = $this->canteenModel->getWeekMenu(4);
        $this->template->friday = $this->canteenModel->getWeekMenu(5);
        $this->template->saturday = $this->canteenModel->getWeekMenu(6);
        $this->template->sunday = $this->canteenModel->getWeekMenu(7);
		
        
    }

  

  
       

}


<?php

namespace App\Presenters;

use Nette;
use App\Forms\SignFormFactory;

use App\Model\UserManager;

class SignPresenter extends BasePresenter {
	/** @var SignFormFactory @inject */
	public $factory;

	/** @var UserManager @inject */
	public $userManager;

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm() {
		$form = $this->factory->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->redirect("Homepage:");
		};
		return $form;
	}

	public function actionIn() {
		if ($this->getUser()->isLoggedIn()) {
			$this->redirect("Homepage:");
		}
	}

	public function actionOut() {
		if ($this->getUser()->isLoggedIn()) {
			$this->getUser()->logout();
			$this->flashMessage("You have been signed out.");
		}
		$this->redirect("in");
	}

	/*
	public function actionAdd() {
		$this->userManager->add("user", "password");
		$this->flashMessage('User has been added.');
		$this->redirect('in');
	}
	*/
}

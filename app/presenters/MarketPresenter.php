<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Presenter;
use App\Model\MarketModel;
use Nette\Utils\Arrays;
    
use Nette\Application\UI\Form;
use Nette\Forms\Controls;

  


  
class MarketPresenter extends ProtectedPresenter
{
	
	public function __construct(marketModel $marketModel)
    {
        $this->marketModel = $marketModel;
        
    }    
                                       
    
    private $marketModel;
    private $database;
    
    
    
    
    
    public function renderDefault()
    {


		$id_user = $this->user->getIdentity()->id_user;

	
		$all_deals1 = $this->marketModel->getAllDeals();
		$all_deals2 = $this->marketModel->getAllDeals();
		$this->template->Myoffers = $this->marketModel->getMyOffers($id_user);
		
		$this->template->candidates = $this->marketModel->getMyOffers($id_user);
		
		
		
		
		$this->template->deals1 =  $all_deals1;
		$this->template->deals2 =  $all_deals2;
		

		$this->template->myCandidates = $this->marketModel->getMyCandidates($id_user);
		$this->template->MyActiveCandidates = $this->marketModel->getMyActiveCandidates($id_user);


	
	
		
	
    }





    public function handleNewCandidate($id_offer){
		
		$id_user = $this->user->getIdentity()->id_user;
		
		$this->marketModel->newCandidate($id_offer, $id_user);

		$this->redirect("default");	
	}




    public function handleAcceptCandidate($id_candidate,$id_offer){


		$accepted = $this->marketModel->IsCandidateAccepted($id_offer);
		
		// only one worker is allowed to switch shift
		if($accepted->getRowCount() >= 1){
			
			//$this->flashMessage('Nie je mozne potvrdit viacero zmien.');
			
		}
		else{
			$this->marketModel->AcceptCandidate($id_candidate);
		}
	
		
	}




    public function handleRefuseCandidate($id_candidate){
		
		$this->marketModel->RefuseCandidate($id_candidate);
		
	}

	
	

    public function handleDeleteCandidate($id_candidate){
		
		$this->marketModel->DeleteCandidate($id_candidate);
		
	}
	
	public function handleDeleteOffer($id_offer){
		
		$this->marketModel->DeleteOffer($id_offer);	
	
	}


    
 //  ****************** formulat na tvorku novej ponuky ****************  
 
	protected function createComponentRegistrationForm()
    {
        $form = new Form;
        $form->addText('date', 'Datum:');
        $form->addText('shift', 'Smena:');
 
        $form->addText('recompense', 'Za:');
 
        $form->addSubmit('login', 'Vytvorit');
        $form->onSuccess[] = array($this, 'registrationFormSucceeded');
        return $form;
    }

    // volá se po úspěšném odeslání formuláře
    public function registrationFormSucceeded($form, $values)
    {
		$id_user = $this->user->getIdentity()->id_user;
		
		
		$this->marketModel->CreateOffer($id_user,$values->date,$values->shift,$values->recompense);	
	
	
		$this->redirect("default");
        //$this->flashMessage('hovno');
  
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      

}


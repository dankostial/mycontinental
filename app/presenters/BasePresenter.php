<?php

namespace App\Presenters;

use Nette;
use App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {
	/** @var Nette\Database\Context
	 * 	@inject
	 */
	public $db;

	public function beforeRender() {

	}
}

<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Presenter;
use App\Model\AttendanceModel;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;   
use Nette\Application\UI\Form;
use Nette\Forms\Controls;

  
class AttendancePresenter extends ProtectedPresenter
{

    
    private $attendanceModel;
    private $database;
	private $year;
	private $month;
	private $month_l;	
	private $day;	
	public function __construct(attendanceModel $attendanceModel)
    {
        $this->attendanceModel = $attendanceModel;
        $this->month =  date("m");
        $this->month_l =  date("M");

		$this->year =  date("Y");
     
    }    
                                       

    
    public function renderDefault()
    {

		$id_user = $this->user->getIdentity()->id_user;

		
		$myAttendance = $this->attendanceModel->getMyAttendance($this->month);
		$this->template->myAttendance =  $myAttendance;
		
		$myAttendance_sum = $this->attendanceModel->getMyAttendance($this->month);
		
		$this->template->month =  $this->month;
		$this->template->month_l =  $this->month_l;
	
		$this->template->year =  $this->year;
	
		
		$sum = 0;
		
		foreach($myAttendance_sum as $attendanceee)
		{
			$sum = $sum + 8;
		}
		
		$this->template->month_hours_sum =  $sum;

		
    }



    public function handleGetMonth($month,$year)
    {
	
		$this->month = $month;
			
		$this->year = $year;
			
			
		if($month == 0){
			
			$this->month = 12;
			
			$this->year = $this->year - 1;
			
			$month = 12;
				
		
		}
		
		if($month == 13){
			
			$this->month = 1;
			
			$this->year = $this->year + 1;
			
			$month = 1;
				
		
		}


		
		
		
		
		switch ($month) {
			case 1:
				$this->month_l = "Januar";
				break;
			case 2:
				$this->month_l = "Februar";
				break;
			case 3:
				$this->month_l = "Marec";
				break;
			case 4:
				$this->month_l = "April";
				break;
			case 5:
				$this->month_l = "Maj";
				break;
			case 6:
				$this->month_l = "Jun";
				break;
			case 7:
				$this->month_l = "Jul";
				break;
			case 8:
				$this->month_l = "August";
				break;
			case 9:
				$this->month_l = "September";
				break;
			case 10:
				$this->month_l = "Oktober";
				break;
			case 11:
				$this->month_l = "November";
				break;
			case 12:
				$this->month_l = "December";
				break;




		}
	
	}

  
    
    
      

}


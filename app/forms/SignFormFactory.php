<?php

namespace App\Forms;


use Nette;
use Nette\Application\UI\Form;
use Nette\Forms\Controls;
use Nette\Security\User;


class SignFormFactory extends Nette\Object
{
	/** @var User */
	private $user;


	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * @return Form
	 */
	public function create()
	{
		$form = new Form;
		$form->addText('username', 'Email:')
			->setRequired('Please enter your username')
			->setAttribute('placeholder', 'username')
			->getControlPrototype()->addClass('form-control');

		$form->addPassword('password', 'Password:')
			->setRequired('Please enter your password')
			->setAttribute('placeholder', 'password')
			->getControlPrototype()->addClass('form-control');

		$checkBox = $form->addCheckbox('remember', 'Keep me signed in');
		$checkBox->getSeparatorPrototype()->setName('div')->addClass($checkBox->getControlPrototype()->type);

		$form->addSubmit('send', 'Sign in')
			->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');

		$form->onSuccess[] = array($this, 'formSucceeded');

		// Bootstrapize
		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['control']['container'] = 'div class=col-sm-6';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$form->getElementPrototype()->class('form-horizontal');

		return $form;
	}

	public function formSucceeded(Form $form, $values)
	{
		if ($values->remember) {
			$this->user->setExpiration('14 days', FALSE);
		} else {
			$this->user->setExpiration('20 minutes', TRUE);
		}

		try {
			$this->user->login($values->username, $values->password);
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
		}
	}

}
